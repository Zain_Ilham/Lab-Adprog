package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

public class Bean implements Veggies {

    public String toString() {
        return "Bean";
    }
}
