package id.ac.ui.cs.advprog.tutorial4.exercise2;

public class Singleton {

    public static Singleton uniqueInstance = new Singleton();

    // TODO Implement me!
    // What's missing in this Singleton declaration?
    private Singleton() {}

    public static Singleton getInstance() {
        // TODO Implement me!
        return uniqueInstance;
    }
}
