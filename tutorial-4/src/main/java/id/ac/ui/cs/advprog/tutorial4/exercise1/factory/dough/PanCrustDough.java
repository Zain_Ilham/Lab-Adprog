package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class PanCrustDough implements Dough {

    public String toString() {
        return "PAN Crust Dough";
    }
}
