package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class BeanTest {

    private Bean bean;

    @Before
    public void setUp() {
        bean = new Bean();
    }

    @Test
    public void testToString() {
        assertEquals("Bean", bean.toString());
    }
}
