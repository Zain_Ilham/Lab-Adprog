package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class PanCrustDoughTest {

    private PanCrustDough panCrustDough;

    @Before
    public void setUp() {
        panCrustDough = new PanCrustDough();
    }

    @Test
    public void testToString() {
        assertEquals("PAN Crust Dough", panCrustDough.toString());
    }

}
