package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

/**
 * Created by user on 28/02/2018.
 */
public class Cucumber extends Food {
    Food food;

    public Cucumber(Food food) {
        this.food = food;
    }

    public String getDescription() {
        return food.getDescription() + ", adding cucumber";
    }

    public double cost() {
        return 0.4 + food.cost();
    }
}
