package id.ac.ui.cs.advprog.tutorial3.decorator.bread;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

/**
 * Created by user on 28/02/2018.
 */
public class ThinBunBurger extends Food {
    public ThinBunBurger() {
        description = "Thin Bun Burger";
    }

    public double cost() {
        return 1.5;
    }
}
