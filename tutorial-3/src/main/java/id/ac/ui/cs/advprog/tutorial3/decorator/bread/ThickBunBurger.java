package id.ac.ui.cs.advprog.tutorial3.decorator.bread;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

/**
 * Created by user on 28/02/2018.
 */
public class ThickBunBurger extends Food {
    public ThickBunBurger() {
        description = "Thick Bun Burger";
    }

    public double cost() {
        return 2.5;
    }
}
