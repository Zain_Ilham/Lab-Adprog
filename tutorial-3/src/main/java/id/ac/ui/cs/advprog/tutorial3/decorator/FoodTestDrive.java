package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.ThickBunBurger;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.BeefMeat;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Cheese;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.ChiliSauce;

public class FoodTestDrive {
    public static void main(String[] args) {
        Food food = new ThickBunBurger();
        System.out.println(food.getDescription() + " $" + food.cost());

        food = new BeefMeat(food);
        food = new ChiliSauce(food);
        food = new Cheese(food);
        System.out.println(food.getDescription() + " $" + food.cost());
    }
}
