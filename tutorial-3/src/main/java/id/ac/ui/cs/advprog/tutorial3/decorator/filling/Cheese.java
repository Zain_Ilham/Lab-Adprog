package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

/**
 * Created by user on 28/02/2018.
 */
public class Cheese extends Food {
    Food food;

    public Cheese(Food food) {
        this.food = food;
    }

    public String getDescription() {
        return food.getDescription() + ", adding cheese";
    }

    public double cost() {
        return 2.0 + food.cost();
    }
}
