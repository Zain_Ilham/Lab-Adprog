package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

/**
 * Created by user on 28/02/2018.
 */
public class ChickenMeat extends Food {
    Food food;

    public ChickenMeat(Food food) {
        this.food = food;
    }

    public String getDescription() {
        return food.getDescription() + ", adding chicken meat";
    }

    public double cost() {
        return 4.5 + food.cost();
    }
}
