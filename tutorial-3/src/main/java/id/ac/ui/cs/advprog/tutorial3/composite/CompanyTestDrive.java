package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;

public class CompanyTestDrive {
    public static void main(String[] args) {
        Company company = new Company();

        Employees ceo = new Ceo("Zain", 350000);
        Employees cto = new Cto("Watson",200000);

        Employees backEndProgrammer = new BackendProgrammer("Zakir", 35000);
        Employees frontEndProgrammer = new FrontendProgrammer("Mary", 45000);
        Employees uiUxDesigner = new UiUxDesigner("Bob", 100000);

        company.addEmployee(ceo);
        company.addEmployee(cto);
        company.addEmployee(frontEndProgrammer);
        company.addEmployee(backEndProgrammer);
        company.addEmployee(uiUxDesigner);

        System.out.println(company.getNetSalaries());

    }
}
