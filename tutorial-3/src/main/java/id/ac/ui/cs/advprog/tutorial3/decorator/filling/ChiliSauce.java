package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

/**
 * Created by user on 28/02/2018.
 */
public class ChiliSauce extends Food {
    Food food;

    public ChiliSauce(Food food) {
        this.food = food;
    }

    public String getDescription() {
        return food.getDescription() + ", adding chile sauce";
    }

    public double cost() {
        return 0.3 + food.cost();
    }
}
