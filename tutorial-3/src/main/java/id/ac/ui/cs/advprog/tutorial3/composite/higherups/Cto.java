package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

/**
 * Created by user on 01/03/2018.
 */
public class Cto extends Employees {
    public Cto(String name, double salary) {
        if (salary < 100000) {
            throw new IllegalArgumentException();
        }

        this.name = name;
        this.salary = salary;
        this.role = "CTO";
    }

    public double getSalary() {
        return salary;
    }
}
