package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

/**
 * Created by user on 28/02/2018.
 */
public class Tomato extends Food {
    Food food;

    public Tomato(Food food) {
        this.food = food;
    }

    public String getDescription() {
        return food.getDescription() + ", adding tomato";
    }

    public double cost() {
        return 0.5 + food.cost();
    }
}
